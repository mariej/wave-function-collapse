/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#pragma once

#include <string>
#include <vector>
#include <png.h>

using namespace std;

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} pixel_t;

using direction = pair<int, int>;
using position = pair<size_t, size_t>;
using pixel_matrix = vector<pixel_t>;
using tile_matrix = vector<pixel_matrix>;

typedef struct {
    pixel_matrix pixels;
    size_t height;
    size_t width;
} bitmap;

typedef struct {
    tile_matrix tile_values;
    size_t height;
    size_t width;
} tilemap;

// (0,0) is the up left corner
#define UP direction(0,-1)
#define RIGHT direction(1,0)
#define DOWN direction(0,1)
#define LEFT direction(-1,0)

#define EMPTY_DIRECTIONS map<direction, vector<size_t>>{{UP, {}}, {RIGHT, {}}, {DOWN, {}}, {LEFT, {}}}

#define RGBA_SIZE 4
#define RGB_SIZE 3
#define BIT_DEPTH 8

#define BLACK pixel_t{205, 205, 177}
#define FAIL_COLOR pixel_t{255, 255, 0}


bool operator==(pixel_t a, pixel_t b);
bool operator!=(pixel_t a, pixel_t b);

/**
 * Converts a bitmap into a tilemap
 * @param tile_size desired size of the tilemap's tiles
 * @param bmp the bitmap to convert
 * @return A vector containing the tilemap conversion
 */
vector<tilemap> bmp_to_tmp(size_t tile_size, const bitmap& bmp);

/**
 * Converts a bitmap into several tilemaps using a sliding window to define the tiles
 * @param tile_size
 * @param bmp
 * @return A vector containing all the resulting tilemaps
 */
vector<tilemap> bmp_to_tmp_sliding(size_t tile_size, const bitmap& bmp);

/**
 * Converts a tilemap into a bitmap
 * @param tile_size size of the tilemap's tiles
 * @param tmp the tilemap to convert
 * @return The bitmap conversion
 */
bitmap tmp_to_bmp(size_t tile_size, const tilemap& tmp);

/**
 * Converts a png image into a bitmap
 * @param height bitmap height
 * @param width bitmap width
 * @param row_pointers pointers to the image pixels
 * @param pixel_size either RGB (3) or RGBA (4)
 * @return The resulting bitmap
 */
bitmap png_to_bmp(size_t height, size_t width, png_bytep *row_pointers, size_t pixel_size);

/**
 * Converts a bitmap into a png image with RGB pixels
 * @param bmp bitmap to convert
 * @return Pointers to each row of the png image
 */
png_bytep* bmp_to_png(const bitmap& bmp);
/**
 * Reads a png image and converts it into a bitmap
 * @param filename path to the png image file
 * @return The resulting bitmap
 */
bitmap read_png(char* filename);
/**
 * Write a png image
 * @param filename path to the resulting png image file
 * @param height
 * @param width
 * @param row_pointers rows to write in the png
 */
void write_png_file(char* filename, size_t height, size_t width, png_bytep *row_pointers);

/**
 * Horizontally reflects the given bitmap
 * @param bmp
 * @return The reflection of the bitmap
 */
bitmap reflect_bmp(const bitmap& bmp);

/**
 * Rotates the given bitmap in the left direction
 * @param bmp
 * @return The left rotation of the bitmap
 */
bitmap rotate_bmp(const bitmap& bmp);

/**
 * Computes all the possible variations of the same bitmap using reflections and optionally rotations and converts them
 * into tilemaps
 * @param bmp the original bitmap
 * @param tile_size the size of the tilemap's tiles
 * @param rotations boolean specifying if rotations can be used or not
 * @param to_tmp function to convert the bitmaps into tilemaps (either with a sliding window or not)
 * @return A vector containing all the variations as tilemaps
 */
vector<vector<tilemap>> bmp_variations(const bitmap& bmp, size_t tile_size, bool rotations, vector<tilemap> (*to_tmp)(size_t, const bitmap&));
