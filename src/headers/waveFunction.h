/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#pragma once

#include <cstdint>
#include <vector>
#include <map>
#include <array>
#include <string>
#include <random>
#include "tile.h"


// Overlap model
class WaveFunction {

private:

    const size_t width, height;             // width x height tile matrix
    const size_t tile_size;
    const bool periodic;
    const bool dynamic_weights;
    vector<vector<size_t>> coefficients;    // contains the state (set of possible tiles) of each region of the output image
    vector<Tile> sample_tiles;              // Distinct sample tiles

    /**
     * Initializes `sample_tiles` with each different tile present in the input pattern
     * @param pattern
     */
    void init_sample_tiles(const tilemap& pattern);

    /**
     * Register all valid neighbors of each tile according to the input pattern
     * @param pattern
     */
    void init_neighbors(const tilemap& pattern);

    /**
     * Initialize the coefficients in a completely unobserved state (all the tiles are valid in each position)
     */
    void init_coefficients();

    /**
     * Find the index of the tile with the given value in the sample set
     * @param v tile value
     * @return The tile index (i.e its id) if it's present in the sample set, -1 otherwise
     */
    int get_sample_index(const pixel_matrix& v) const;

    /**
     * Computes the Shannon entropy at position [x,y]
     * @param x
     * @param y
     * @return The Shannon entropy
     */
    double entropy(size_t x, size_t y) const;

    /**
     * Computes the minimal entropy over all image positions
     * @return 0 if all entropies are null, otherwise the smallest entropy
     */
    double find_min_entropy() const;

    /**
     * Fills `directions` with the valid directions around the given position in the image
     * @param coords position
     * @param directions
     */
    void valid_directions(const pair<size_t, size_t>& coords, vector<direction>& directions) const;

    /**
     * Fills `directions` with the valid directions around the given position within a w x h image depending on the
     * periodicity
     * @param w image width
     * @param h image height
     * @param coords position
     * @param directions
     */
    void valid_neighbor_directions(size_t w, size_t h, const pair<size_t, size_t>& coords, vector<direction>& directions) const;

    /**
     * Used when the algorithm reaches a dead-end
     */
    void abort();


public:

    /**
     * Initializes a new Wave Function with completely unobserved elements
     */
    WaveFunction(size_t output_width, size_t output_height, size_t tile_size, const bitmap& pattern, bool periodic, bool rotations, bool sliding_window, bool dynamic_weights);
    WaveFunction(const WaveFunction& other) = delete;

    /**
     * Checks if all entropies are non null and randomly chooses a position among the ones having the minimal entropy
     * @param chosen_coords set to the randomly selected position with minimal entropy
     * @return True if contradictory state (all entropies are null), false if no contradiction
     */
    bool choose_collapse_position(position& chosen_coords) const;

    /**
     * Collapses the tile at the given position into a definite state
     * @param coords position of the tile to collapse
     */
    void collapse(const position& coords);

    /**
     * Propagates the information gained after collapsing tile at the given coordinates
     * @param coords last collapse position
     */
    void propagate(const pair<size_t, size_t>& coords);

    /**
     * Checks if all the elements are completely observed
     * @return True if fully observed, false otherwise
     */
    bool fully_observed() const;

    /**
     * Provides the texture coordinates of each tile in the result
     * @return A vector of all the texture coordinates
     */
    vector<position> get_texture_result() const;

    /**
     * Provides the rows of the generated image
     * @return A pointer to each image rows
     */
    png_bytep* get_png_result() const;
};
