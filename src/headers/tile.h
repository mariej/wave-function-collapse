/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#pragma once

#include <cstdint>
#include <set>
#include <map>
#include <string>
#include <vector>
#include "utility.h"


class Tile {

public:

    const size_t t_id;                          // Unique id generated as the tile's index in the sample_tiles set
    const size_t size;
    double weight;                              // Number of occurrences in the sample image
    const pixel_matrix value;                   // Matrix containing each tile pixel
    pair<float, float> texture_coords;
    map<direction, vector<size_t>> neighbors;   // Maps all valid neighbors to each direction

    Tile(size_t id, size_t size, pixel_matrix val, double weight, map<direction, vector<size_t>> n, pair<float, float>  texture_coords);

    /**
     * Check if tile t is valid at direction d
     * @param id Neighbor to check
     * @param d Direction of the neighbor
     * @return Compatibility of the tiles
     */
    bool compatible(size_t id, const direction& d) const;

    Tile operator=(const Tile& t);
    bool operator!=(const Tile& t) const;
    bool operator==(const Tile& b) const;
    bool operator==(const pixel_matrix& val) const;
    bool operator==(size_t id) const;
    bool operator<(const Tile& b) const;
};