/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "../stb_image/stb_image.h"
#include "headers/shader.h"
#include "headers/waveFunction.h"

#define QUAD_SIZE 4
#define QUAD_VERTICES 6
#define VERTEX_OFFSET 5
#define SCR_WIDTH 200
#define SCR_HEIGHT 200


// Callback function called every time the window dimensions change
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

// Query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window);


int main() {
    // =================================================================================================================
    // Initialize and configure GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // Create a window object
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "WFC", nullptr, nullptr);
    if(!window) {
        cerr << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    // Tell GLFW to call ´framebuffer_size_callback´ on every window resize
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // GLAD: load all OpenGL function pointers
    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        cerr << "Failed to initialize GLAD" << endl;
        return -1;
    }

    // Build and compile the shader program
    Shader ourShader("/home/marie/Documents/WFC/wfc-opengl/src/shaders/shader.vert",
                     "/home/marie/Documents/WFC/wfc-opengl/src/shaders/shader.frag");


    // =================================================================================================================
    // Load and create texture
    unsigned int texture;

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // load image, create texture and generate mipmaps
    char *texture_file = "../textures/Village.png";
    int texture_width, texture_height, nrChannels;
    unsigned char *data = stbi_load(texture_file, &texture_width, &texture_height, &nrChannels, 0);
    if(data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_width, texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        cerr << "Failed to load texture" << endl;
    }
    stbi_image_free(data);

    // =================================================================================================================
    const unsigned int wfc_width = 10;
    const unsigned int wfc_height = 10;
    const unsigned int tile_size = 2;

    bitmap pattern = read_png(texture_file);

    WaveFunction wf(wfc_width, wfc_height, tile_size, pattern, false, false, false, false);

    // Run the WFC algorithm
    while(!wf.fully_observed()) {
        position collapse_coords;

        // Get coords of min_entropy and check if contradictory state
        if(wf.choose_collapse_position(collapse_coords)) {
            cerr << "Reached a dead-end!" << endl;
            return -1;
        }

        // Collapse tile with min_entropy
        wf.collapse(collapse_coords);

        // Propagate information gained
        wf.propagate(collapse_coords);
    }
    // Output png
    write_png_file("../result.png", tile_size*wfc_height, tile_size*wfc_width, wf.get_png_result());

    // =================================================================================================================
    vector<position> output = wf.get_texture_result();

    float vertices[QUAD_SIZE * VERTEX_OFFSET * wfc_width * wfc_height];

    float tile_width = 2.0f / (wfc_width);
    float tile_height = 2.0f / (wfc_height);

    // =================================================================================================================

    size_t vertices_index = 0;
    for(size_t y = 0; y < wfc_height; ++y) {
        for(size_t x = 0; x < wfc_width; ++x) {
            // Create 4 quad corners
            for(size_t j = 0; j < 2; ++j) {
                for(size_t i = 0; i < 2; ++i) {
                    // Position
                    vertices[vertices_index] = (x + i) * tile_width - 1.0f;
                    vertices[vertices_index + 1] = 1.0f - ((y + j) * tile_height);
                    vertices[vertices_index + 2] = 0.0f;
                    // Texture coords
                    pair<float, float> texture_coords = output[(y * wfc_width) + x];
                    vertices[vertices_index + 3] = (texture_coords.first + i) * tile_size / (float)texture_width;
                    vertices[vertices_index + 4] = (texture_coords.second + j) * tile_size / (float)texture_height;

                    vertices_index += VERTEX_OFFSET;
                }
            }
        }
    }

    unsigned int indices[QUAD_VERTICES * wfc_width * wfc_height];
    size_t index = 0;
    for(size_t i = 0; i < QUAD_SIZE * wfc_width * wfc_height; i += QUAD_SIZE) {
        // First triangle
        indices[index] = i;         // top left
        indices[index + 1] = i + 1; // top right
        indices[index + 2] = i + 3; // bottom right
        // Second triangle
        indices[index + 3] = i;     // top left
        indices[index + 4] = i + 3; // bottom right
        indices[index + 5] = i + 2; // bottom left

        index += QUAD_VERTICES;
    }

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s)
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // =================================================================================================================
    ourShader.use(); // activate the shader before setting uniforms

    // Render loop to keep drawing until we tell GLFW to stop
    while (!glfwWindowShouldClose(window)) {
        // Input
        processInput(window);

        // Bind textures on corresponding texture units
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);

        // Draw
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6 * wfc_width * wfc_height, GL_UNSIGNED_INT, nullptr);

        // GLFW: swap buffers (to avoid flickering of the image) and poll IO events (keys pressed/released, mouse moved etc.)
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate();

    return 0;
}


void processInput(GLFWwindow *window) {
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}
