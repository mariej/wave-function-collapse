/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#include "headers/utility.h"


bool operator==(pixel_t a, pixel_t b) {
    return a.red == b.red && a.green == b.green && a.blue == b.blue;
}

bool operator!=(pixel_t a, pixel_t b) {
    return !(a == b);
}

vector<tilemap> bmp_to_tmp(size_t tile_size, const bitmap& bmp) {

    size_t tmp_height = bmp.height / tile_size;
    size_t tmp_width = bmp.width / tile_size;
    tile_matrix m(tmp_height * tmp_width, pixel_matrix(tile_size * tile_size));

    // Traverse the bitmap "tile by tile"
    size_t tmp_y = 0;
    for(size_t y = 0; y < bmp.height; y += tile_size) {
        size_t tmp_x = 0;
        for(size_t x = 0; x < bmp.width; x += tile_size) {
            // Create the tile
            for(size_t j = 0; j < tile_size; ++j) {
                for(size_t i = 0; i < tile_size; ++i) {
                    m[tmp_y * tmp_width + tmp_x][j * tile_size + i] = bmp.pixels[(y + j) * bmp.width + (x + i)];
                }
            }
            ++tmp_x;
        }
        ++tmp_y;
    }

    tilemap tmp;
    tmp.width = tmp_width;
    tmp.height = tmp_height;
    tmp.tile_values.swap(m);

    return vector<tilemap>(1, tmp);
}

vector<tilemap> bmp_to_tmp_sliding(size_t tile_size, const bitmap& bmp) {

    size_t tmp_height = bmp.height / tile_size;
    size_t tmp_width = bmp.width / tile_size;
    vector<tile_matrix> m(tile_size * tile_size, tile_matrix(tmp_height * tmp_width, pixel_matrix(tile_size * tile_size)));

    // Traverse the bitmap "tile by tile"
    size_t tmp_y = 0;
    for(size_t y = 0; y < bmp.height; y += tile_size) {
        size_t tmp_x = 0;
        for(size_t x = 0; x < bmp.width; x += tile_size) {
            // Create the tile
            for(size_t j = 0; j < tile_size; ++j) {
                for(size_t i = 0; i < tile_size; ++i) {
                    // Slide the window
                    for(size_t d_y = 0; d_y < tile_size; ++d_y) {
                        const size_t index_y = (y + d_y + j) % bmp.height;
                        for(size_t d_x = 0; d_x < tile_size; ++d_x) {
                            const size_t index_x = (x + d_x + i) % bmp.width;
                            m[d_y * tile_size + d_x][tmp_y * tmp_width + tmp_x][j * tile_size + i] =
                                    bmp.pixels[index_y * bmp.width +index_x];
                        }
                    }
                }
            }
            ++tmp_x;
        }
        ++tmp_y;
    }

    vector<tilemap> tmps(tile_size);
    for(size_t a = 0; a < tile_size; ++a) {
        tilemap tmp;
        tmp.width = tmp_width;
        tmp.height = tmp_height;
        tmp.tile_values.swap(m[a]);
        tmps.push_back(tmp);
    }

    return tmps;
}

bitmap tmp_to_bmp(size_t tile_size, const tilemap& tmp) {
    bitmap bmp;
    bmp.width = tile_size * tmp.width;
    bmp.height = tile_size * tmp.height;

    pixel_matrix pixels(bmp.height * bmp.width);

    // Traverse each tile in the tilemap
    for(size_t y = 0; y < tmp.height; ++y) {
        for(size_t x = 0; x < tmp.width; ++x) {
            // Traverse each tile pixel
            for(size_t tile_y = 0; tile_y < tile_size; ++tile_y) {
                for(size_t tile_x = 0; tile_x < tile_size; ++tile_x) {
                    pixels[((tile_size * y) + tile_y) * bmp.width + ((tile_size * x) + tile_x)] =
                            tmp.tile_values[y * tmp.width + x][tile_y * tile_size + tile_x];
                }
            }
        }
    }
    bmp.pixels.swap(pixels);
    return bmp;
}

bitmap png_to_bmp(size_t height, size_t width, png_bytep *row_pointers, size_t pixel_size) {

    pixel_matrix pixels(height * width);

    // Traverse each row
    for(size_t y = 0; y < height; ++y) {
        // Convert each pixel in the row
        size_t bmp_width = 0;
        for(size_t x = 0; x < pixel_size * width; x += pixel_size) {
            pixel_t p;
            p.red = row_pointers[y][x];
            p.green = row_pointers[y][x+1];
            p.blue = row_pointers[y][x+2];

            pixels[y * width + bmp_width] = p;
            bmp_width++;
        }
    }
    bitmap pattern;
    pattern.height = height;
    pattern.width = width;
    pattern.pixels = pixels;

    return pattern;
}

png_bytep* bmp_to_png(const bitmap& bmp) {

    auto *row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * bmp.height);

    // Traverse each bitmap row
    for(size_t y = 0; y < bmp.height; ++y) {
        row_pointers[y] = (png_bytep) malloc(sizeof(uint8_t) * RGB_SIZE * bmp.width);

        // Convert each pixel in the row
        int width = 0;
        for(size_t x = 0; x < bmp.width; ++x) {
            row_pointers[y][width] = bmp.pixels[y * bmp.width + x].red;
            row_pointers[y][width+1] = bmp.pixels[y * bmp.width + x].green;
            row_pointers[y][width+2] = bmp.pixels[y * bmp.width + x].blue;
            width += RGB_SIZE;
        }
    }
    return row_pointers;
}

bitmap read_png(char* filename) {

    FILE *fp = fopen(filename, "rb");
    if(!fp) {
        abort();
    }
    // Check if it's a png
    int read_bytes = 8;
    unsigned char header[8];
    fread(header, 1, read_bytes, fp);
    if(png_sig_cmp(header, 0, read_bytes)) {
        abort();
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,nullptr, nullptr, nullptr);
    if(!png_ptr) {
        abort();
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        abort();
    }

    png_infop end_info = png_create_info_struct(png_ptr);
    if(!end_info) {
        png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
        abort();
    }

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, read_bytes);

    png_read_info(png_ptr, info_ptr);

    size_t width = png_get_image_width(png_ptr, info_ptr);
    size_t height = png_get_image_height(png_ptr, info_ptr);
    int color_type = png_get_color_type(png_ptr, info_ptr);

    size_t pixel_size;
    if(color_type == PNG_COLOR_TYPE_RGB) {
        pixel_size = RGB_SIZE;
    } else if(color_type == PNG_COLOR_TYPE_RGBA) {
        pixel_size = RGBA_SIZE;
    }

    png_bytep *row_pointers;
    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    for(size_t y = 0; y < height; ++y) {
        row_pointers[y] = (png_bytep) malloc(png_get_rowbytes(png_ptr, info_ptr));
    }

    png_read_image(png_ptr, row_pointers);

    png_read_end(png_ptr, end_info);
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    fclose(fp);

    bitmap bmp = png_to_bmp(height, width, row_pointers, pixel_size);
    for(size_t y = 0; y < height; ++y) {
        free(row_pointers[y]);
    }
    free(row_pointers);
    return bmp;
}

void write_png_file(char* filename, size_t height, size_t width, png_bytep *row_pointers) {

    FILE * fp;
    fp = fopen(filename, "wb");
    if(!fp) {
        abort();
    }

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if(!png_ptr) {
        abort();
    }

    png_infop info_ptr = png_create_info_struct (png_ptr);
    if(!info_ptr) {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        abort();
    }

    // Set image attributes
    png_set_IHDR(png_ptr,
                 info_ptr,
                 width,
                 height,
                 BIT_DEPTH,
                 PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);

    // Write image data
    png_init_io(png_ptr, fp);
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, row_pointers);

    for(size_t y = 0; y < height; y++) {
        png_free(png_ptr, row_pointers[y]);
    }
    png_free(png_ptr, row_pointers);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
}

bitmap reflect_bmp(const bitmap& bmp) {
    pixel_matrix pixels(bmp.height * bmp.width);

    for(size_t y = 0; y < bmp.height; ++y) {
        for(size_t x = 0; x < bmp.width; ++x) {
            pixels[y * bmp.width + x] = bmp.pixels[(bmp.width * y) + (bmp.width - 1 - x)];
        }
    }

    bitmap reflection;
    reflection.height = bmp.height;
    reflection.width = bmp.width;
    reflection.pixels = pixels;

    return reflection;
}

bitmap rotate_bmp(const bitmap& bmp) {
    pixel_matrix pixels(bmp.height * bmp.width);

    for(size_t y = 0; y < bmp.height; ++y) {
        for(size_t x = 0; x < bmp.width; ++x) {
            pixels[(bmp.width - 1 - x) * bmp.height + y] = bmp.pixels[y * bmp.width + x];
        }
    }

    bitmap rotation;
    rotation.height = bmp.width;
    rotation.width = bmp.height;
    rotation.pixels = pixels;

    return rotation;
}

vector<vector<tilemap>> bmp_variations(const bitmap& bmp, size_t tile_size, bool rotations,
                                       vector<tilemap> (*to_tmp)(size_t, const bitmap&)) {
    vector<vector<tilemap>> variations;

    variations.push_back(to_tmp(tile_size, bmp));

    if(rotations) {
        const bitmap reflected      = reflect_bmp(bmp);
        const bitmap rotated        = rotate_bmp(bmp);
        const bitmap upside_down    = rotate_bmp(rotated);
        const bitmap rotated_right  = rotate_bmp(upside_down);
        const bitmap rotated_refl   = reflect_bmp(rotated);
        const bitmap upside_refl    = reflect_bmp(upside_down);
        const bitmap rot_right_refl = reflect_bmp(rotated_right);

        variations.push_back(to_tmp(tile_size, reflected));
        variations.push_back(to_tmp(tile_size, rotated));
        variations.push_back(to_tmp(tile_size, upside_down));
        variations.push_back(to_tmp(tile_size, rotated_right));
        variations.push_back(to_tmp(tile_size, rotated_refl));
        variations.push_back(to_tmp(tile_size, upside_refl));
        variations.push_back(to_tmp(tile_size, rot_right_refl));
    }
    return variations;
}
