/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#include <utility>
#include <algorithm>
#include <headers/tile.h>


Tile::Tile(size_t id, size_t size, pixel_matrix  val, double weight, map<direction, vector<size_t>> n, pair<float, float> texture_coords) :
        t_id(id), size(size), value(move(val)), weight(weight), neighbors(move(n)), texture_coords(std::move(texture_coords)) {}

bool Tile::compatible(const size_t id, const direction& d) const {
    // Check if id is in the set of neighbors
    return count(neighbors.at(d).begin(), neighbors.at(d).end(), id);
}

Tile Tile::operator=(const Tile& t) {
    return Tile(t.t_id, t.size, t.value, t.weight, t.neighbors, t.texture_coords);
}

bool Tile::operator==(const Tile& b) const {
    return t_id == b.t_id;
}

bool Tile::operator!=(const Tile& b) const {
    return t_id != b.t_id;
}

bool Tile::operator==(const pixel_matrix & val) const {
    for(size_t y = 0; y < size; ++y) {
        for(size_t x = 0; x < size; ++x) {
            if(value[y * size + x] != val[y * size + x]) {
                return false;
            }
        }
    }
    return true;
}

bool Tile::operator==(size_t id) const {
    return t_id == id;
}

bool Tile::operator<(const Tile& b) const {
    return weight < b.weight;
}

