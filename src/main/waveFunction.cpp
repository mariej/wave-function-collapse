/*
    Copyright 2020 Marie Jaillot

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/


#include <cmath>
#include <limits>
#include <stack>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <memory>
#include "headers/waveFunction.h"


WaveFunction::WaveFunction(size_t output_width, size_t output_height, size_t tile_size,
                           const bitmap& pattern, bool periodic, bool rotations, bool sliding_window, bool dynamic_weights) :
        width(output_width), height(output_height), tile_size(tile_size),
        coefficients(width * height, vector<size_t>()), periodic(periodic), dynamic_weights(dynamic_weights) {

    // Expand tile set
    vector<vector<tilemap>> variations =
            bmp_variations(pattern, tile_size, rotations, sliding_window ? &bmp_to_tmp_sliding : &bmp_to_tmp);

    for(const vector<tilemap>& tmps : variations) {
        for(const tilemap& tmp : tmps) {
            init_sample_tiles(tmp);
            init_neighbors(tmp);
        }
    }

    init_coefficients();

    // Complete sample_tiles with a black tile and a "fail" tile
//    sample_tiles.emplace_back(sample_tiles.size(), tile_size, pixel_matrix(tile_size * tile_size, FAIL_COLOR), -1, EMPTY_DIRECTIONS);
//    sample_tiles.emplace_back(sample_tiles.size(), tile_size, pixel_matrix(tile_size * tile_size, BLACK), -1, EMPTY_DIRECTIONS);
}

void WaveFunction::init_sample_tiles(const tilemap& pattern) {
    for(size_t y = 0; y < pattern.height; ++y) {
        for(size_t x = 0; x < pattern.width; ++x) {

            int index = get_sample_index(pattern.tile_values[y * pattern.width + x]);
            // If the tile is not registered yet, add it to the sample tiles
            if(index == -1) {
                // Update the index
                index = sample_tiles.size();
                map<direction, vector<size_t>> neighbors = EMPTY_DIRECTIONS;
                pair<float, float> texture_coords(x, y);
                sample_tiles.emplace_back(index, tile_size, pattern.tile_values[y * pattern.width + x], 0, neighbors, texture_coords);
            }
            // Increment the tile weight
            sample_tiles[index].weight += 1.0;
        }
    }
}

void WaveFunction::init_neighbors(const tilemap& pattern) {
    for(int y = 0; y < pattern.height; ++y) {
        for(int x = 0; x < pattern.width; ++x) {
            // Get the current tile's index
            int cur_id = get_sample_index(pattern.tile_values[y * pattern.width + x]);

            // Get the valid directions around the current position
            vector<direction> directions;
            valid_neighbor_directions(pattern.width, pattern.height, make_pair(x, y), directions);

            // Add neighbor in each valid direction around the tile
            for(direction dir : directions) {
                // Compute neighbor position
                size_t n_x = ((x + dir.first) % (int)pattern.width + (int)pattern.width) % pattern.width;
                size_t n_y = ((y + dir.second) % (int)pattern.height + (int)pattern.height) % pattern.height;

                vector<size_t> cur_neighbors = sample_tiles[cur_id].neighbors.at(dir);
                size_t n_id = get_sample_index(pattern.tile_values[n_y * pattern.width + n_x]);

                // If a new neighbor is found, add it
                if(!count(cur_neighbors.begin(), cur_neighbors.end(), n_id)) {
                    sample_tiles[cur_id].neighbors.at(dir).push_back(n_id);
                }
            }
        }
    }
}

void WaveFunction::init_coefficients() {
    // Initialize coefficients to unobserved state
    vector<vector<size_t>> coeffs(height * width, vector<size_t>());
    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            for(const Tile& t : sample_tiles) {
                coeffs[y * width + x].push_back(t.t_id);
            }
        }
    }
    coefficients = coeffs;
}

int WaveFunction::get_sample_index(const pixel_matrix& v) const {
    auto it = find(sample_tiles.begin(), sample_tiles.end(), v);

    // If element was found
    if(it != sample_tiles.end()) {
        return distance(sample_tiles.begin(), it);
    }
    return -1;
}

double WaveFunction::entropy(size_t x, size_t y) const {
    double sum_weights = 0.0;

    for(size_t id : coefficients[y * width + x]) {
        sum_weights += sample_tiles[id].weight;
    }

    double entropy = 0.0;
    for(size_t id : coefficients[y * width + x]) {
        double probability = sample_tiles[id].weight / sum_weights;
        entropy -= probability * log(probability);
    }
    return entropy;
}

double WaveFunction::find_min_entropy() const {
    double min_entropy = numeric_limits<double>::infinity();

    // Find minimal entropy
    for(size_t j = 0; j < height; ++j) {
        for(size_t i = 0; i < width; ++i) {
            double cur_entropy = entropy(i, j);

            // Check if all entropies are null and update min entropy found
            if(cur_entropy != 0 && (cur_entropy < min_entropy)) {
                min_entropy = cur_entropy;
            }
        }
    }

    if(min_entropy == numeric_limits<double>::infinity()) {
        min_entropy = 0;
    }

    return min_entropy;
}

bool WaveFunction::choose_collapse_position(position& chosen_coords) const {

    double min_entropy = find_min_entropy();

    // Check for contradiction
    if(min_entropy == 0) {
        return true;
    }

    vector<position> candidates;
    // List all positions having the minimal entropy
    for(size_t j = 0; j < height; ++j) {
        for(size_t i = 0; i < width; ++i) {
            double cur_entropy = entropy(i, j);

            if(cur_entropy == min_entropy) {
                candidates.emplace_back(i, j);
            }
        }
    }

    // Generate a random index
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distr(0, candidates.size()-1);
    int rdm_index = distr(gen);

    // Randomly select a position among the ones with minimal entropy
    chosen_coords = candidates[rdm_index];

    return false;
}

void WaveFunction::valid_neighbor_directions(const size_t w, const size_t h, const position& coords, vector<direction>& directions) const {
    if(coords.second > 0 || periodic) {
        directions.emplace_back(UP);
    }
    if(coords.first < w - 1 || periodic) {
        directions.emplace_back(RIGHT);
    }
    if(coords.second < h - 1 || periodic) {
        directions.emplace_back(DOWN);
    }
    if(coords.first > 0 || periodic) {
        directions.emplace_back(LEFT);
    }
}

void WaveFunction::valid_directions(const position& coords, vector<direction>& directions) const {
    if(coords.second > 0) {
        directions.emplace_back(UP);
    }
    if(coords.first < width - 1) {
        directions.emplace_back(RIGHT);
    }
    if(coords.second < height - 1) {
        directions.emplace_back(DOWN);
    }
    if(coords.first > 0) {
        directions.emplace_back(LEFT);
    }
}

void WaveFunction::collapse(const position& coords) {

    // Compute the sum of the weights of the admitted tiles
    double total_weights = 0;
    for(size_t id : coefficients[coords.second * width + coords.first]) {
        total_weights += sample_tiles[id].weight;
    }

    // Generate a random number in the range [0, total_weights)
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> distribution(0.0, total_weights-1.0);
    double remaining_dist = distribution(gen);

    // Randomly select a tile according to their weights (linear "search")
    for(size_t id : coefficients[coords.second * width + coords.first]) {
        remaining_dist -= sample_tiles[id].weight;

        if(remaining_dist < 0) {
            // Update weight if necessary
            if(dynamic_weights) {
                sample_tiles[id].weight *= 0.9;
            }
            // Collapse
            vector<size_t> collapsed = {id};
            coefficients[coords.second * width + coords.first].swap(collapsed);
            return;
        }
    }
}

void WaveFunction::propagate(const position& coords) {
    // Initialize a stack of coords to keep track of the visited tiles
    stack<position> stack_coords;
    stack_coords.push(coords);

    while(!stack_coords.empty()) {
        position cur_coords = stack_coords.top();
        stack_coords.pop();


        // Get the possible tiles at the current position
        vector<size_t> cur_ids = coefficients[cur_coords.second * width + cur_coords.first];

        // Get the valid directions around the current position
        vector<direction> directions;
        valid_directions(cur_coords, directions);

        // Check the neighbors in each direction
        for(direction dir : directions) {
            position next_coords =
                    make_pair(cur_coords.first + dir.first, cur_coords.second + dir.second);

            // Iterate over all valid neighbors of the current tile and maintain a list of the neighbors to keep
            vector<size_t> updated;
            for(size_t neighbor_id : coefficients[next_coords.second * width + next_coords.first]) {

                // Check compatibility with each tile of the current position
                bool valid = false;
                for(size_t cur_id : cur_ids) {
                    // The tile is still valid if it is compatible with at least one potential tile
                    if(sample_tiles[cur_id].compatible(neighbor_id, dir)) {
                        valid = true;
                        break;
                    }
                }
                if(!valid) {
                    // Keep track of coordinates if neighbor removed
                    stack_coords.push(next_coords);
                } else {
                    updated.emplace_back(neighbor_id);
                }
            }

            // Update weight if necessary
            if(dynamic_weights && updated.size() == 1) {
                sample_tiles[updated[0]].weight *= 0.9;
            }
            // Update the valid neighbors
            coefficients[next_coords.second * width + next_coords.first].swap(updated);

            // Abort and mark the position with a fail tile if there are no possibilities left
//            if(coefficients[next_coords.second * width + next_coords.first].empty()) {
//                abort();
//                return;
//            }
        }
    }
}

void WaveFunction::abort() {
    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            if(coefficients[y * width + x].empty()) {
                // Failed tile if no possibility
                vector<size_t> failed(1, sample_tiles.size()-2);
                coefficients[y * width + x].swap(failed);
            } else if(coefficients[y * width + x].size() > 1) {
                // Black tile if not collapsed
                vector<size_t> black(1, sample_tiles.size()-1);
                coefficients[y * width + x].swap(black);
            }
        }
    }
}

bool WaveFunction::fully_observed() const {
    // Check if all positions are collapsed
    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            if(coefficients[y * width + x].size() != 1) {
                return false;
            }
        }
    }
    return true;
}

png_bytep* WaveFunction::get_png_result() const {

    tile_matrix m(height * width, pixel_matrix(tile_size * tile_size));

    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            size_t id = coefficients[y * width + x][0];
            m[y * width + x] = sample_tiles[id].value;
        }
    }
    tilemap result;
    result.height = height;
    result.width = width;
    result.tile_values.swap(m);

    bitmap bmp_result = tmp_to_bmp(tile_size, result);

    return bmp_to_png(bmp_result);
}

vector<position> WaveFunction::get_texture_result() const {
    vector<position> output(width * height);

    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            size_t id = coefficients[y * width + x][0];
            output[y * width + x] = sample_tiles[id].texture_coords;
        }
    }

    return output;
}




