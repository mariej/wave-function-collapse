# Wave Function Collapse

The goal of this project is to modify the original [Wave Function Collapse](https://github.com/mxgmn/WaveFunctionCollapse) 
technique to make it work with irregular quad grids, inspired by Oskar Stalberg's work on Townscaper. 
Ideally, this project would also be implemented for 3D irregular voxel grids. 
Some of the images used here come from the original Wave Function Collapse repository linked above.

# How it works

The overlap model of the wave function collapse algorithm produces images that are locally similar to an input image.

Here is an example of an input:  <img src="results/Link.png" alt="Input" width="50" height="50"/> used to generate this image:  <img src="results/Link_gen_3x3.png" alt="Output" width="110" height="110"/>


The image is interpreted as a grid of NxN tiles: <img src="results/Tile_grid.png" width="150" height="150"/>


We proceed as follows:

First, we read the input image, listing every NxN tile encountered and creating an adjacency list for each tile, which
will be used as constraints.

<img src="results/Neighbors.png" width="200" height="200"/>
<img src="results/Adjacency_list.png" width="250" height="250"/>

Once all the tiles are listed, we create the wave function, an array representing the current state of the output
image. Each cell contains a set of tiles that could fit in, so it will initially contain all the listed tiles because at
the beginning, any tile can fit any position.

Then comes the main iteration of the algorithm where we will choose a tile for each position:

- The first step consists in observing the wave function and finding the position with the lowest entropy (i.e. the
position having the least possible tiles to choose from). Then we randomly choose one of the tiles listed at this 
position and collapse it. This means that we have chosen the final state of the cell.

- The second step propagates the information gained when we collapsed the tile. We will iterate over the neighbors of 
our collapsed position and only keep the tiles that are compatible with it, according to the adjacency list of the 
collapsed tile. Evey time an incompatible tile is removed from a position, this information must also be propagated. 
This step stops when all the positions impacted are updated.

This iteration will keep going until the wave function is fully collapsed (i.e. each position is assigned a single tile),
or if we encountered a contradiction, which is a position where no tile can be placed, making it impossible to complete
the output image.

To make sure that our output images are similar to the input image, we assign weights to the tiles, making them more
likely to be chosen in the collapse process if they appear more often in the input image.

## Expanding the data

In order to generate more interesting images and increase our chances of finding a fully collapsed wave function, three
different option can be activated.

The first consists in generating additional tiles by rotating and reflecting the original ones. Thus, each original tile
can produce (at most, since we dont keep duplicates) seven new versions of it.

The second option is to consider the image as periodic to allow tiles to admit more possible neighbors.

Finally, we can also use a sliding window to list all the input tiles, moving it pixel by pixel, to generate more tiles.

Here are some results to illustrate the different options using 2x2 tiles on this input image: <img src="results/ColoredCity.png" width="30" height="30"/>

<figcaption>Using only reflection (default usage)</figcaption><img src="results/city_simple_2x2.png" width="180" height="180"/>
<img src="results/city_simple_2x2(1).png" width="180" height="180"/>
<img src="results/city_simple_2x2(2).png" width="180" height="180"/>

<figcaption>Using rotations</figcaption><img src="results/city_rotations_2x2.png" width="180" height="180"/>
<img src="results/city_rotations_2x2(1).png" width="180" height="180"/>
<img src="results/city_rotations_2x2(2).png" width="180" height="180"/>

<figcaption>Using periodicity</figcaption><img src="results/city_periodic_2x2.png" width="180" height="180"/>
<img src="results/city_periodic_2x2(1).png" width="180" height="180"/>
<img src="results/city_periodic_2x2(2).png" width="180" height="180"/>

<figcaption>Using a sliding window</figcaption><img src="results/city_sliding_2x2.png" width="180" height="180"/>
<img src="results/city_sliding_2x2(1).png" width="180" height="180"/>
<img src="results/city_sliding_2x2(2).png" width="180" height="180"/>

<figcaption>Using periodicity and rotations</figcaption><img src="results/city_periodic_rotations_2x2.png" width="180" height="180"/>
<img src="results/city_periodic_rotations_2x2(1).png" width="180" height="180"/>
<img src="results/city_periodic_rotations_2x2(2).png" width="180" height="180"/>

<figcaption>Using rotations and a sliding window</figcaption><img src="results/city_rotations_sliding_2x2.png" width="180" height="180"/>
<img src="results/city_rotations_sliding_2x2(1).png" width="180" height="180"/>
<img src="results/city_rotations_sliding_2x2(2).png" width="180" height="180"/>

<figcaption>Using periodicity and a sliding window</figcaption><img src="results/city_periodic_sliding_2x2.png" width="180" height="180"/>
<img src="results/city_periodic_sliding_2x2(1).png" width="180" height="180"/>
<img src="results/city_periodic_sliding_2x2(2).png" width="180" height="180"/>

<figcaption>Using periodicity, rotations and a sliding window</figcaption><img src="results/city_periodic_rotations_sliding_2x2.png" width="180" height="180"/>
<img src="results/city_periodic_rotations_sliding_2x2(3).png" width="180" height="180"/>
<img src="results/city_periodic_rotations_sliding_2x2(2).png" width="180" height="180"/>
