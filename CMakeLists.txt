cmake_minimum_required(VERSION 3.10)
project(wfc)

set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}-fsanitize=address")

# Find the needed libraries
find_package(OpenGL REQUIRED)

message(STATUS "Found GLFW3 in ${GLFW_INCLUDE_DIR}")

# Define the include DIRs
include_directories(
        "${CMAKE_SOURCE_DIR}/src"
        "${CMAKE_SOURCE_DIR}/OpenGLDep/includes"
        "${CMAKE_SOURCE_DIR}/stb_image"
)

# Build GLFW along with our project
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
add_subdirectory(${CMAKE_SOURCE_DIR}/OpenGLDep/DepSrc/glfw-3.3.2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Set compilation path for all source folders
file(GLOB STB_IMAGE ./stb_image/*)
file(GLOB CPP_SOURCES ./src/main/*.cpp)
file(GLOB C_SOURCES ./src/C_src/*.c)
file(GLOB HEADERS ./src/headers/*.h)
file(GLOB SHADERS ./src/shaders/*)
add_executable(wfc ${STB_IMAGE} ${HEADERS} ${C_SOURCES} ${SHADERS} ${CPP_SOURCES})


find_package(PNG REQUIRED)
include_directories(${PNG_INCLUDE_DIR})
target_link_libraries(wfc glfw ${PNG_LIBRARY})
